<?php
session_start();
?>

<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Mon Compte</title>
  <link rel="stylesheet" type="text/css" media="all" href="css/mystyle.css"/>
</head>
<body>
<header>
  <form method="POST" action="myController.php">
    <input type="hidden" name="action" value="disconnect">
    <button class="btn-logout form-btn">Déconnexion</button>
  </form>

  <h2><?php echo $_SESSION["connected_user"]["prenom"]; ?> <?php echo $_SESSION["connected_user"]["nom"]; ?> - Mon
    compte</h2>
</header>

<section>

  <article>
    <div style="width:40%" class="fieldset">
      <div class="fieldset_label">
        <span>Vos informations personnelles</span>
      </div>
      <div class="field">
        <label>Login : </label><span><?php echo $_SESSION["connected_user"]["login"]; ?></span>
      </div>
      <div class="field">
        <label>Profil : </label><span><?php echo $_SESSION["connected_user"]["profil_user"]; ?></span>
      </div>
    </div>
  </article>

  <article>
    <div style="width:40%" class="fieldset">
      <div class="fieldset_label">
        <span>Votre compte</span>
      </div>
      <div class="field">
        <label>N° compte : </label><span><?php echo $_SESSION["connected_user"]["numero_compte"]; ?></span>
      </div>
      <div class="field">
        <label>Solde : </label><span><?php echo $_SESSION["connected_user"]["solde_compte"]; ?> &euro;</span>
      </div>
    </div>
  </article>
</section>
<article>
  <form method="POST" target=_blank action="myController.php">
    <input type="hidden" name="action" value="transfert_page">
    <input type="hidden" name="account_to_charge" value="<?php echo $_SESSION["connected_user"]["numero_compte"] ?>">
    <div class="fieldset" style="background-color: #D70000">
      <center>
        <h1 style="color:white">TRANSFÉRER DE L'ARGENT</h1>
        <input type="image" src='static/transfert.png' alt="submit form"/>
      </center>
    </div>
  </form>
</article>

<article>
  <form method="POST" target=_blank action="myController.php">
    <input type="hidden" name="action" value="messagerie">
    <div class="fieldset" style="background-color: #007D8F">
      <center>
        <h1 style="color:white">MESSAGERIE</h1>
        <input type="image" src='static/messagerie.png' alt="submit form"/>
      </center>
    </div>
  </form>
</article>

<?php if ($_SESSION["connected_user"]["profil_user"] == 'EMPLOYE') {
  ?>
<article>
  <form method="POST" target=_blank action="myController.php">
    <input type="hidden" name="action" value="userList">
    <div class="fieldset" style="background-color: #99CC00">
      <center>
        <h1 style="color:white">CLIENTS</h1>
        <input type="image" src='static/client.png' alt="submit form"/>
      </center>
    </div>
  </form>
</article>
<?php
}?>
</section>

</body>
</html>
