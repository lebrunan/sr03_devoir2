<?php
require_once('myModel.php');

session_start();

// URL de redirection par défaut (si pas d'action ou action non reconnue)
$url_redirect = "index.php";

if (isset($_REQUEST['action'])) {

    if ($_REQUEST['action'] == 'authenticate') {
        /* ======== AUTHENT ======== */
        if (!isset($_REQUEST['login']) || !isset($_REQUEST['mdp']) || $_REQUEST['login'] == "" || $_REQUEST['mdp'] == "") {
            // manque login ou mot de passe
            $url_redirect = "vw_login.php?nullvalue";

        } else {
            $utilisateur = findUserByLoginPwd($_REQUEST['login'], $_REQUEST['mdp']);

            if ($utilisateur == false) {
                // echec authentification
                $url_redirect = "vw_login.php?badvalue";

            } else {
                // authentification réussie
                $_SESSION["connected_user"] = $utilisateur;
                $_SESSION["listeUsers"] = findAllUsers();
                $url_redirect = "vw_moncompte.php";
            }
        }

    } else if ($_REQUEST['action'] == 'disconnect') {
        /* ======== DISCONNECT ======== */
        unset($_SESSION["connected_user"]);
        $url_redirect = "vw_login.php";

    } else if ($_REQUEST['action'] == 'userList') {
        /* ======== DISCONNECT ======== */
        $url_redirect = "vw_admin.php";

    } else if ($_REQUEST['action'] == 'transfert') {
        /* ======== TRANSFERT ======== */
        if (currentUserCanDoTransfert($_REQUEST["account_to_charge"])) {
            if (is_numeric($_REQUEST['montant'])) {
                if ($_REQUEST["destination"] !== $_REQUEST["account_to_charge"]) {
                    if (doesAccountNumberExist($_REQUEST["destination"])) {
                        transfert($_REQUEST['destination'], $_REQUEST["account_to_charge"], $_REQUEST['montant']);
                        $_SESSION["connected_user"]["solde_compte"] = $_SESSION["connected_user"]["solde_compte"] - $_REQUEST['montant'];
                        $url_redirect = "vw_transfert.php?trf_ok&account_to_charge=" . $_REQUEST["account_to_charge"];
                        $_SESSION["listeUsers"] = findAllUsers();
                    } else {
                        $url_redirect = "vw_transfert.php?error=accountDoesNotExist&account_to_charge=" . $_REQUEST["account_to_charge"];
                    }
                } else {
                    $url_redirect = "vw_transfert.php?error=sameAccount&account_to_charge=" . $_REQUEST["account_to_charge"];
                }
            } else {
                $url_redirect = "vw_transfert.php?error=notNumeric&account_to_charge=" . $_REQUEST["account_to_charge"];
            }
        } else {
            $url_redirect = "vw_transfert.php?error=noAccess&account_to_charge=" . $_REQUEST["account_to_charge"];
        }

    } else if ($_REQUEST['action'] == 'sendmsg') {
        /* ======== MESSAGE ======== */
        $msg_subject = htmlspecialchars($_REQUEST['sujet']);
        $msg_body = htmlspecialchars($_REQUEST['corps']);
        addMessage($_REQUEST['to'], $_SESSION["connected_user"]["id_user"], $msg_subject, $msg_body);
        $url_redirect = "vw_messagerie.php?msg_ok";

    } else if ($_REQUEST['action'] == 'messagerie') {
        /* ======== MESSAGE ======== */
        $_SESSION['messagesRecus'] = findMessagesInbox($_SESSION["connected_user"]["id_user"]);
        $url_redirect = "vw_messagerie.php";

    } else if ($_REQUEST['action'] == 'transfert_page') {
    /* ======== MESSAGE ======== */
        $url_redirect = "vw_transfert.php?account_to_charge=" . $_REQUEST["account_to_charge"];

    }


}
function currentUserCanDoTransfert($accountToCharge) {
    return $accountToCharge == $_SESSION["connected_user"]["numero_compte"] || isCurrentUserEmploye();
}

function isCurrentUserEmploye() {
    return $_SESSION["connected_user"]["profil_user"] == 'EMPLOYE';
}
header("Location: $url_redirect");

?>
