<?php
session_start();
?>

<article>
    <form method="POST" action="myController.php">
        <input type="hidden" name="action" value="transfert">
        <input type="hidden" name="account_to_charge" value="<?php echo $_REQUEST['account_to_charge']?>">
        <div class="fieldset">
            <div class="fieldset_label">
                <span>Transférer de l'argent</span>
            </div>
            <div class="field">
                <label>N° compte destinataire : </label><input type="text" size="20" name="destination">
            </div>
            <div class="field">
                <label>Montant à transférer : </label><input type="text" size="10" name="montant">
            </div>
            <button class="form-btn">Transférer</button>
            <?php
            if (isset($_REQUEST["trf_ok"])) {
                echo '<p>Virement effectué avec succès.</p>';
            }
            if (isset($_REQUEST["error"])) {
                if ($_REQUEST["error"] == "notNumeric") {
                    echo '<p>Vous avez saisi un montant incorrect</p>';
                } else if ($_REQUEST["error"] == "sameAccount") {
                    echo '<p>Vous ne pouvez pas faire un virement à vous même </p>';
                } else if ($_REQUEST["error"] == "accountDoesNotExist") {
                    echo '<p>Le compte destinatire n\'existe pas.</p>';
                } else if ($_REQUEST["error"] == "noAccess") {
                    echo '<p>Vous n\'avez pas la permission d\'effectuer ce virement.</p>';
                }
            }
            ?>
        </div>
    </form>
</article>
