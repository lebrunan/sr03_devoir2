<?php
session_start();
if ($_SESSION["connected_user"]["profil_user"] != 'EMPLOYE') {
    header("myController.php");
}
?>
<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Messages</title>
  <link rel="stylesheet" type="text/css" media="all" href="css/mystyle.css"/>
</head>
<body>
<header>
  <h1> Administration </h1>
</header>

<div class="liste">
  <table>
    <tr>
      <th>Nom</th>
      <th>Prénom</th>
      <th>Solde</th>
      <th>Effectuer un virement</th>
    </tr>
      <?php
      foreach ($_SESSION['listeUsers'] as $user) {
          echo '<tr>';
          echo '<form method="POST" target=_blank action="myController.php">';
          echo '<input type="hidden" name="action" value="transfert_page">';
          echo '<input type="hidden" name="account_to_charge" value="' . $user["numero_compte"] . '">';
          echo '<td>' . $user['nom']  . '</td>';
          echo '<td>' . $user['prenom'] . '</td>';
          echo '<td>' . $user['solde_compte'] . '</td>';
          echo '<td> <input type="image" alt="submit form" style="width: 1vw;" src="static/transfert.png"/> </td>';
          echo '</form>';
          echo '</tr>';
      }
      ?>
  </table>
</div>