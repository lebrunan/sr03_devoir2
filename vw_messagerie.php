<?php
session_start();
?>

<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Messages</title>
  <link rel="stylesheet" type="text/css" media="all" href="css/mystyle.css"/>
</head>
<body>
<header>
  <h2><?php echo $_SESSION["connected_user"]["prenom"]; ?> <?php echo $_SESSION["connected_user"]["nom"]; ?> -
    Messagerie </h2>
</header>

<section>
  <article>
    <form method="POST" action="myController.php">
      <input type="hidden" name="action" value="sendmsg">

      <div class="field">
        <label>Destinataire : </label>
        <select name="to">
            <?php
            foreach ($_SESSION['listeUsers'] as $id => $user) {
                echo '<option value="' . $id . '">' . $user['nom'] . ' ' . $user['prenom'] . '</option>';
            }
            ?>
        </select>
      </div>
      <div class="field">
        <label>Sujet : </label><input type="text" size="20" name="sujet">
      </div>
      <div class="field">
        <label>Message : </label><textarea name="corps" cols="25" rows="3""></textarea>
      </div>
      <button class="form-btn">Envoyer</button>
    </form>
      <?php
      if (isset($_REQUEST["msg_ok"])) {
          echo '<p>Message envoyé avec succès.</p>';
      }
      ?>
    <div style="margin-top: 5vh" class="liste">
      <table>
        <tr>
          <th>Expéditeur</th>
          <th>Sujet</th>
          <th>Message</th>
        </tr>
          <?php
          foreach ($_SESSION['messagesRecus'] as $cle => $message) {
              echo '<tr>';
              echo '<td>' . $message['nom'] . ' ' . $message['prenom'] . '</td>';
              echo '<td>' . $message['sujet_msg'] . '</td>';
              echo '<td>' . $message['corps_msg'] . '</td>';
              echo '</tr>';
          }
          ?>
      </table>
    </div>

  </article>
</section>
</body>
</html>
